import os
import sys

from PIL import Image

filename = sys.argv[1]
name, ext = os.path.splitext(filename)
image = Image.open(filename)
width, height = image.size

#degrees = len(sys.argv) > 2 and int(sys.argv[2]) or 30
degrees = len(sys.argv) > 2 and float(sys.argv[2]) or 30
rotates = int(360/degrees)
filters = {
    'cu': Image.BICUBIC, 'ne': Image.NEAREST, 'li': Image.BILINEAR
}
filter = len(sys.argv) > 3 and sys.argv[3] or 'cu'
master = Image.new(mode='RGBA', size=(width*rotates, height), color=(0,0,0,0))
for i in range(0, rotates, 1):
    out = image.rotate(i*degrees, filters[filter])
    master.paste(out, (i*width, 0))
master.save(name + '.' + filter + '.' + str(degrees) + ext)
